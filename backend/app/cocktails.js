const express = require("express");
const Cocktail = require("../models/Cocktail");
const upload = require("../multer").cocktail;
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");

const router = express.Router();

router.get("/", auth, async (req, res) => {
    try {
        if (req.user.role === "user") {
            const cocktailsUser = await Cocktail.find({published: true}).populate("user", "displayName");
            return res.send(cocktailsUser);
        } else if (req.user.role === "admin") {
            const cocktails = await Cocktail.find().populate("user", "displayName");
            return res.send(cocktails);
        }
    } catch (e) {
        res.sendStatus(500);
    }
});

router.get("/mycocktails", auth, async (req, res) => {
    try {
        const myCocktails = await Cocktail.find({user: req.user}).populate("user", "displayName");
        return res.send(myCocktails);
    } catch (e) {
        res.sendStatus(500);
    }
});

router.get("/:id", async (req, res) => {
    try {
        const cocktail = await Cocktail.findOne({_id: req.params.id}).populate("user", "displayName");
        return res.send(cocktail);
    } catch (e) {
        res.sendStatus(500);
    }
});

router.post("/", auth, upload.single("image"), async (req, res) => {
    try {
        const cocktail = new Cocktail({
            title: req.body.title,
            recipe: req.body.recipe,
            user: req.user,
            image: req.file ? req.file.filename : null,
            ingredients: JSON.parse(req.body.ingredients),
        });

        await cocktail.save();
        return res.send(cocktail);
    } catch (e) {
        res.sendStatus(500);
    }
});

router.post("/admin/:id", auth, permit("admin"), async (req, res) => {
    try {
        const cocktail = await Cocktail.findOne({_id: req.params.id});

        cocktail.published = true;
        await cocktail.save();

        return res.send(cocktail);
    } catch (e) {
        res.sendStatus(500);
    }
});

router.delete("/admin/:id", auth, permit("admin"), async (req, res) => {
    try {
        await Cocktail.deleteOne({_id: req.params.id});
        return res.send("Deleted");
    } catch (e) {
        res.sendStatus(500);
    }
});

module.exports = router;
