const express = require("express");
const User = require("../models/User");
const config = require("../config");
const axios = require("axios");
const {downloadAvatar} = require("../utils");
const {nanoid} = require("nanoid");
const upload = require("../multer").avatar;

const router = express.Router();

router.get("/", async (req, res) => {
    try {
        const user = await User.find();
        return res.send(user);
    } catch (e) {
        return res.status(500);
    }
});

router.post("/", upload.single("avatar"), async (req, res) => {
    try {
        const user = new User({
            email: req.body.email,
            password: req.body.password,
            displayName: req.body.displayName,
            avatar: req.file ? req.file.filename : null,
        });

        user.generateToken();
        await user.save();
        return res.send(user);
    } catch (error) {
        return res.status(400).send(error);
    }
});

router.post('/sessions', async (req, res) => {
    const user = await User.findOne({email: req.body.email});

    if (!user) {
        return res.status(401).send({message: 'Credentials are wrong!'});
    }

    const isMatch = await user.checkPassword(req.body.password);

    if (!isMatch) {
        return res.status(401).send({message: 'Credentials are worng!'});
    }

    user.generateToken();
    await user.save({validateBeforeSave: false});

    res.send({message: 'Username and password correct!', user});
});



router.delete("/sessions", async (req, res) => {
    const token = req.get("Authorization");
    const success = {message: "Success"};

    if (!token) return res.send(success);

    const user = await User.findOne({token});

    if (!user) return res.send(success);

    user.generateToken();

    await user.save({validateBeforeSave: false});

    return res.send(success);
});


router.post("/", upload.single("image"), async (req, res) => {
    const userData = {
        email: req.body.email,
        password: req.body.password,
        displayName: req.body.displayName,
    };
    try {
        if (req.file) {
            userData.avatarImage = "uploads/" + req.file.filename;
        }

        const user = new User(userData);

        user.generateToken();
        await user.save();
        return res.send(user);
    } catch (error) {
        return res.status(400).send(error);
    }
});




module.exports = router;
