import axiosCocktail from "./axiosCocktail";

const API = {
    registerUser: (userData) => {
        const data = new FormData();

        Object.keys(userData).forEach((key) => {
            data.append(key, userData[key]);
        });

        return axiosCocktail.post("/users", data);
    },
};

export default API;
