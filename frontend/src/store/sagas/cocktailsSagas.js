import {put, takeEvery} from "redux-saga/effects";
import axiosCocktail from "../../axiosCocktail";
import {historyPush} from "../actions/historyActions";
import {
    FetchCocktailsFailure,
    FetchCocktailsRequest,
    FetchCocktailsSuccess,
    FetchSoloCocktailSuccess,
    FetchSoloCocktailFailure,
    FetchSoloCocktailRequest,
    PostCocktailsFailure,
    PostCocktailsRequest,
    PostCocktailsSuccess,
    FetchMyCocktailsSuccess,
    FetchMyCocktailsFailure,
    FetchMyCocktailsRequest,
    DeleteCocktailsSuccess,
    DeleteCocktailsFailure,
    PublishedCocktailsSuccess,
    PublishedCocktailsFailure,
    DeleteCocktailsRequest,
    PublishedCocktailsRequest,
} from "../actions/cocktailsActions";
import {addNotification} from "../actions/notifierActions";

export function* fetchCocktails() {
    try {
        const response = yield axiosCocktail.get("/cocktails");
        yield put(FetchCocktailsSuccess(response.data));
    } catch (error) {
        yield put(FetchCocktailsFailure(error.response.data));
    }
}

export function* fetchSoloCocktail({payload: id}) {
    try {
        const response = yield axiosCocktail.get("/cocktails/" + id);
        yield put(FetchSoloCocktailSuccess(response.data));
    } catch (error) {
        yield put(FetchSoloCocktailFailure(error.response.data));
    }
}

export function* fetchMyCocktails() {
    try {
        const response = yield axiosCocktail.get("/cocktails/mycocktails");
        yield put(FetchMyCocktailsSuccess(response.data));
    } catch (error) {
        yield put(FetchMyCocktailsFailure(error.response.data));
    }
}

export function* postCocktail({payload: cocktailData}) {
    try {
        yield axiosCocktail.post("/cocktails", cocktailData);
        yield put(PostCocktailsSuccess());
        yield put(historyPush("/"));
        yield put(
            addNotification({message: "Your cocktail is being moderated", options: {variant: "success"}})
        );
    } catch (error) {
        yield put(PostCocktailsFailure(error.response.data));
    }
}

export function* deleteCocktail({payload: id}) {
    try {
        yield axiosCocktail.delete("/cocktails/admin/" + id);
        yield put(DeleteCocktailsSuccess());
        yield put(FetchCocktailsRequest());
        yield put(FetchMyCocktailsRequest());

        yield put(addNotification({message: "Your cocktail Deleted", options: {variant: "success"}}));
    } catch (error) {
        yield put(DeleteCocktailsFailure(error.response.data));
    }
}

export function* publishedCocktail({payload: id}) {
    try {
        yield axiosCocktail.post("/cocktails/admin/" + id);
        yield put(PublishedCocktailsSuccess());
        yield put(FetchCocktailsRequest());
        yield put(FetchMyCocktailsRequest());

        yield put(addNotification({message: "Cocktail Published", options: {variant: "success"}}));
    } catch (error) {
        yield put(PublishedCocktailsFailure(error.response.data));
    }
}

const cocktailsSagas = [
    takeEvery(FetchCocktailsRequest, fetchCocktails),
    takeEvery(FetchMyCocktailsRequest, fetchMyCocktails),
    takeEvery(FetchSoloCocktailRequest, fetchSoloCocktail),
    takeEvery(PostCocktailsRequest, postCocktail),
    takeEvery(DeleteCocktailsRequest, deleteCocktail),
    takeEvery(PublishedCocktailsRequest, publishedCocktail),
];

export default cocktailsSagas;
