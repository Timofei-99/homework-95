import React, {useState} from "react";
import {useDispatch} from "react-redux";
import {Button, Grid, Typography} from "@material-ui/core";
import FileInput from "../../components/UI/Form/FileInput";
import FormElement from "../../components/UI/Form/FormElement";
import {PostCocktailsRequest} from "../../store/actions/cocktailsActions";

const AddNewCocktail = () => {
    const dispatch = useDispatch();
    const [cocktail, setCocktail] = useState({
        title: "",
        recipe: "",
        image: "",
        ingredients: [{title: "", amount: ""}],
    });

    const inputChangeHandler = (e) => {
        const {name, value} = e.target;

        setCocktail((prev) => ({...prev, [name]: value}));
    };

    const fileChangeHandler = (e) => {
        const name = e.target.name;
        const file = e.target.files[0];

        setCocktail((prevState) => ({
            ...prevState,
            [name]: file,
        }));
    };

    const changeIngredient = (i, name, value) => {
        setCocktail((prev) => {
            const ingCopy = {
                ...prev.ingredients[i],
                [name]: value,
            };

            const a = prev.ingredients.map((ing, index) => {
                if (index === i) {
                    return ingCopy;
                }
                return ing;
            });

            return {...prev, ingredients: a};
        });
    };

    const addIngredient = () => {
        setCocktail((prev) => {
            const ingredients = [...prev.ingredients];
            const newIngredients = [...ingredients, {title: "", amount: ""}];

            return {...prev, ingredients: newIngredients};
        });
    };

    const deleteIngredient = (index) => {
        setCocktail((prev) => {
            const ingredients = [...prev.ingredients];

            ingredients.splice(index, 1);

            return {...prev, ingredients};
        });
    };

    const submitFormHandler = (e) => {
        e.preventDefault();
        const formData = new FormData();

        const ingredients = JSON.stringify(cocktail.ingredients);

        formData.append("title", cocktail.title);
        formData.append("image", cocktail.image);
        formData.append("recipe", cocktail.recipe);
        formData.append("ingredients", ingredients);

        dispatch(PostCocktailsRequest(formData));
    };

    return (
        <Grid container spacing={2} direction="column" component="form" onSubmit={submitFormHandler}>
            <Typography variant="h3">Add New Cocktails</Typography>
            <Grid item>
                <FormElement
                    label="Title"
                    type="title"
                    onChange={inputChangeHandler}
                    name="title"
                    value={cocktail.title}
                    required
                />
            </Grid>
            <Grid item>
                {cocktail.ingredients.map((ing, i) => (
                    <Grid item container key={i} spacing={2}>
                        <FormElement
                            label="Title"
                            name="title"
                            onChange={(e) => changeIngredient(i, "title", e.target.value)}
                        />
                        <FormElement
                            label="Amount"
                            name="amount"
                            onChange={(e) => changeIngredient(i, "amount", e.target.value)}
                        />
                        <Button onClick={() => deleteIngredient(i)}>X</Button>
                    </Grid>
                ))}
                <Button variant="outlined" onClick={addIngredient} style={{margin: "20px 0"}}>
                    +
                </Button>
            </Grid>
            <Grid item>
                <FormElement
                    multiline
                    rows={3}
                    label="Recipe"
                    type="recipe"
                    onChange={inputChangeHandler}
                    name="recipe"
                    value={cocktail.recipe}
                    required
                />
            </Grid>
            <Grid item xs>
                <FileInput name="image" label="Image" onChange={fileChangeHandler}/>
            </Grid>

            <Grid item xs>
                <Button type="submit" variant="contained" color="primary">
                    Add
                </Button>
            </Grid>
        </Grid>
    );
};

export default AddNewCocktail;
