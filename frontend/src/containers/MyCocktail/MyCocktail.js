import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {FetchMyCocktailsRequest} from "../../store/actions/cocktailsActions";
import Cocktail from "../AllCocktail/Cocktail";
import {Grid} from "@material-ui/core";

const MyCocktail = () => {
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(FetchMyCocktailsRequest());
    }, [dispatch]);

    const myCocktails = useSelector((state) => state.cocktails.myCocktails);

    return (
        <Grid container spacing={2}>
            {myCocktails.map((cocktail) => (
                <Cocktail
                    key={cocktail._id}
                    id={cocktail._id}
                    title={cocktail.title}
                    image={cocktail.image}
                    author={cocktail.user.displayName}
                    published={cocktail.published}
                />
            ))}
        </Grid>
    );
};

export default MyCocktail;
