import React from "react";
import {Button, Card, CardContent, CardHeader, CardMedia, Grid, makeStyles} from "@material-ui/core";
import imageNotAvailable from "../../assets/images/imageNotAvailable.png";
import {apiURL} from "../../config";
import {Link} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {DeleteCocktailsRequest, PublishedCocktailsRequest} from "../../store/actions/cocktailsActions";

const useStyles = makeStyles({
    card: {
        height: "100%",
        border: "2px solid black",
    },
    media: {
        height: 0,
        paddingTop: "90.25%",
    },
    link: {
        textDecoration: "none",
        textAlign: "center",
        margin: "20px 0",
    },
});

const Cocktail = ({title, image, author, id, published}) => {
    const dispatch = useDispatch();
    const classes = useStyles();
    const user = useSelector((state) => state.users.user);

    let cardImage = imageNotAvailable;

    if (image) {
        cardImage = apiURL + "/" + image;
    }

    return (
        <Grid item xs sm md={6} lg={3} style={{margin: "15px 0"}}>
            <Grid item component={Link} to={"/cocktail/" + id} className={classes.link}>
                <Card className={classes.card}>
                    <CardHeader title={title}/>
                    <CardMedia image={cardImage} title={title} className={classes.media}/>
                    <CardContent>Author:{author}</CardContent>
                    <>{published && published === "false" ? <p>Не опубликован</p> : <p>Опубликован</p>}</>
                </Card>
            </Grid>
            {user.role === "admin" && (
                <Grid item style={{margin: "20px"}}>
                    {published !== "true" && (
                        <Button onClick={() => dispatch(PublishedCocktailsRequest(id))} variant="outlined">
                            To published
                        </Button>
                    )}
                    <Button onClick={() => dispatch(DeleteCocktailsRequest(id))} variant="outlined">
                        Delete
                    </Button>
                </Grid>
            )}
        </Grid>
    );
};

export default Cocktail;
