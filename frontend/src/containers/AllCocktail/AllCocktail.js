import React, {useEffect} from "react";
import {useDispatch} from "react-redux";
import {FetchCocktailsRequest} from "../../store/actions/cocktailsActions";
import {useSelector} from "react-redux";
import {Grid} from "@material-ui/core";
import Cocktail from "./Cocktail";

const AllCocktail = () => {
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(FetchCocktailsRequest());
    }, [dispatch]);

    const cocktails = useSelector((state) => state.cocktails.cocktails);
    return (
        <Grid container spacing={2}>
            {cocktails.map((cocktail) => (
                <Cocktail
                    key={cocktail._id}
                    id={cocktail._id}
                    title={cocktail.title}
                    author={cocktail.user.displayName}
                    image={cocktail.image}
                    published={cocktail.published}
                />
            ))}
        </Grid>
    );
};

export default AllCocktail;
